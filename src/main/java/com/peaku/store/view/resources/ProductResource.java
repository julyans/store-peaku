/**
 * 
 */
package com.peaku.store.view.resources;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Esta clase representa la interfaz para consumir los servicios de productos
 * @author Julián Mora
 *
 */

@RestController
@RequestMapping("/api/product")
public class ProductResource {
	
	
}
