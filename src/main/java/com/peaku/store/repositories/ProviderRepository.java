package com.peaku.store.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.peaku.store.model.Provider;

/**
 * Se crea la interface de Provider heredando los metodos del CRUD de JpaRepository
 * @author Julián Mora
 *
 */
public interface ProviderRepository extends JpaRepository<Provider, Integer> {
	
	public Provider findByProviderNit(@Param("id") int id);
}
