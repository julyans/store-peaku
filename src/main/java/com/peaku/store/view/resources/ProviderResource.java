/**
 * 
 */
package com.peaku.store.view.resources;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.peaku.store.model.Provider;
import com.peaku.store.services.ProviderService;
import com.peaku.store.view.resources.vo.ProviderVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Esta clase representa la interfaz para consumir los servicios de proveedores
 * @author Julián Mora
 *
 */

@RestController
@RequestMapping("/api/provider")
@Api(tags = "provider")
public class ProviderResource {
	
	private final ProviderService providerService;
	
	public ProviderResource(ProviderService providerService) {
		this.providerService = providerService;
	}
	
	@PostMapping
	@ApiOperation(value = "Crear Proveedor", notes = "Servicio para crear proveedores")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Proveedor creado correctamente"), 
			@ApiResponse(code = 404, message = "Solicitud inválida")})
	public ResponseEntity<Provider> createProvider(@RequestBody ProviderVO providerVo){
		Provider provider = new Provider();
		provider.setProvider__nit(providerVo.getProvider__nit());
		provider.setProvider__name(providerVo.getProvider__name());
		provider.setProvider__phone(providerVo.getProvider__phone());
		provider.setProvider__email(providerVo.getProvider__email());
		
		return new ResponseEntity<>(this.providerService.create(provider), HttpStatus.CREATED);
	} 
	
	@PutMapping("/{id}")
	@ApiOperation(value = "Actualizar Proveedor", notes = "Servicio para actualizar proveedores")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Proveedor actualizado correctamente"), 
			@ApiResponse(code = 404, message = "Proveedor no encontrado")})
	public ResponseEntity<Provider> updateProvider(@PathVariable("id") int id, ProviderVO providerVo){
		Provider provider = this.providerService.findByProviderNit(id);
		if(provider == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else {
			provider.setProvider__nit(providerVo.getProvider__nit());
			provider.setProvider__name(providerVo.getProvider__name());
			provider.setProvider__phone(providerVo.getProvider__phone());
			provider.setProvider__email(providerVo.getProvider__email());
		}
		return new ResponseEntity<>(this.providerService.update(provider), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "{Eliminar Proveedor", notes = "Servicio para eliminar proveedores")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Proveedor eliminado correctamente"), 
			@ApiResponse(code = 404, message = "Proveedor no encontrado")})
	public void removeProvider(@PathVariable("id") int id) {
		Provider provider = this.providerService.findByProviderNit(id);
		if(provider != null) {
			this.providerService.delete(provider);
		}
	}
	
	@GetMapping
	@ApiOperation(value = "Obtener Proveedores", notes = "Servicio para listar proveedores")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Proveedores obtenidos correctamente"), 
			@ApiResponse(code = 404, message = "Proveedores no encontrados")})
	public ResponseEntity<List<Provider>> findAll(){
		return ResponseEntity.ok(this.providerService.findAll());
	}
}
