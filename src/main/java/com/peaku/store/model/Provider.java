/**
 * 
 */
package com.peaku.store.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Esta clase representa la tabla de proveedores
 * @author Julián Mora
 *
 */
@Data
@EqualsAndHashCode(exclude = "product")
@Entity
@NamedQuery(name = "Provider.findByProviderNit", query = "select p from Provider p where p.id =:id")
public class Provider {

	/**
	 *  Se mapea la tabla provider...
	 */
	
	@Id
	@GeneratedValue
	int id;
	String provider__nit;
	String provider__name;
	String provider__phone;
	String provider__email;
	
	@OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
	private Set<Product> product;
	
	public Provider() {
		// TODO Auto-generated constructor stub
	}

	public Provider(int id, String provider__nit, String provider__name, String provider__phone, String provider__email,
			Set<Product> product) {
		super();
		this.id = id;
		this.provider__nit = provider__nit;
		this.provider__name = provider__name;
		this.provider__phone = provider__phone;
		this.provider__email = provider__email;
		this.product = product;
	}

}
