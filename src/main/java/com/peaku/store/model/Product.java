/**
 * 
 */
package com.peaku.store.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import lombok.Data;

/**
 * Esta clase representa la tabla de producto
 * @author Julián Mora
 *
 */
@Data
@Entity
@NamedQuery(name = "Product.findByProductProviderNit", query = "select prod from Product prod JOIN Provider prov where prod.provider__nit =:provider__nit")
public class Product {

	/**
	 *  Se mapea la tabla product...
	 */
	
	@Id
	@GeneratedValue
	int id;
	String product__code;
	String provider__nit;
	float product__price;
	String product__name;
	
	@ManyToOne
    @JoinColumn
    private Provider provider;
    
	public Product() {
		// TODO Auto-generated constructor stub
	}

	public Product(int id, String product__code, String provider__nit, float product__price, String product__name,
			Provider provider) {
		super();
		this.id = id;
		this.product__code = product__code;
		this.provider__nit = provider__nit;
		this.product__price = product__price;
		this.product__name = product__name;
		this.provider = provider;
	}

}
