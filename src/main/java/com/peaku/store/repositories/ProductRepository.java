/**
 * 
 */
package com.peaku.store.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.peaku.store.model.Product;


/**
 *  * Se crea el repositorio de product heredando los metodos para el CRUD de JpaRepository
 * @author Julián Mora
 *
 */
public interface ProductRepository extends JpaRepository<Product, Integer>{
	
	// @Query("select prod from Product prod JOIN Provider prov where prod.provider__nit =:provider__nit")
	public List<Product>  findByProductProviderNit(@Param("provider__nit") String provider__nit);
}
