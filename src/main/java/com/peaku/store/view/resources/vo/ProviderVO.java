/**
 * 
 */
package com.peaku.store.view.resources.vo;

import lombok.Data;

/**
 * Clase que representa la tabla provider en terminos de virtual object para la capa de cosumo de API Rest
 * @author Julián Mora
 *
 */

@Data
public class ProviderVO {
	String provider__nit;
	String provider__name;
	String provider__phone;
	String provider__email;
}
