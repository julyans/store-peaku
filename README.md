Instalacion

1- Crear base de datos mysql con nombre <store>
2- Ejecutar script sql:

CREATE USER 'devplay'@'localhost' IDENTIFIED BY 'devplay';
GRANT ALL PRIVILEGES ON *.* TO 'devplay'@'localhost';
FLUSH PRIVILEGES;

3- Ejecutar servidor mvn clean install tomcat:run

4- Ejecutar script sql para registros de prueba:



					/* ================================ */
					/* 		INSERT PROVIDERS */
					/* ================================ */

INSERT INTO provider (id, provider__email, provider__name, provider__nit, provider__phone ) 
	values (1,'CARMELO__V@GMAIL.COM', 'CARMELO VALENCIA', '123456', '3143563423' );

INSERT INTO provider (id, provider__email, provider__name, provider__nit, provider__phone) 
	values (2,'JORGE__R@GMAIL.COM', 'JORGE RODRIGUEZ', '1234567', '3137892343' );

INSERT INTO provider (id, provider__email, provider__name, provider__nit, provider__phone) 
	values (3,'GERENCIA@TODOLAPTOP.COM', 'TODO LAPTOP S.A.S', '12345678', '3137492343' );

INSERT INTO provider (id, provider__email, provider__name, provider__nit, provider__phone) 
	values (4,'CARMEN__M@GMAIL.COM', 'CARME MENDEZ S.A.S', '12345679', '3147892342' );
					
					/* ================================ */
					/* 		INSERT PRODUCTS */
					/* ================================ */

/* PROVIDERS__NIT = 123456 */
INSERT INTO product (id, product__code, product__name, product__price, provider__nit, provider_id) 
	values (1,'PRD1', 'HARD DRIVE SSD 500GB', '450000', '123456', 1);

INSERT INTO product (id, product__code, product__name, product__price, provider__nit,  provider_id) 
	values (2,'PRD2', 'MOUSE INALAMBRICO', '95000', '123456', 1);

INSERT INTO product (id, product__code, product__name, product__price, provider__nit,  provider_id) 
	values (3,'PRD3', 'TECLADO CABLEADO', '75000', '123456', 1);

/* PROVIDERS__NIT = 1234567 */
INSERT INTO product (id, product__code, product__name, product__price, provider__nit, provider_id) 
	values (4,'PRD4', 'SILLA GERENCIAL', '350000', '1234567',2);

INSERT INTO product (id, product__code, product__name, product__price, provider__nit, provider_id) 
	values (5,'PRD5', 'ESCRITORIO DOBLE ADMINISTRATIVO', '560000', '1234567',2);

INSERT INTO product (id, product__code, product__name, product__price, provider__nit, provider_id) 
	values (6,'PRD6', 'PAPELERA DE RECICLAJE GRANDE', '87000', '1234567',  2);

