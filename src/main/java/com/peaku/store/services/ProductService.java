/**
 * 
 */
package com.peaku.store.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.peaku.store.model.Product;
import com.peaku.store.repositories.ProductRepository;

/**
 * Clase que define los servicios de product
 * @author Julián Mora
 *
 */

@Service
@Transactional(readOnly = true)
public class ProductService {

	private final ProductRepository productRepository;
	
	public ProductService(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}
	
	/**
	 * Método que realiza la acción de guardar los datos de un producto
	 * @param product
	 * @return
	 */
	@Transactional
	public Product create(Product product) {
		return this.productRepository.save(product);
	}
	
	/**
	 * Método que realiza la acción de actualizar los datos de un producto
	 * @param product
	 * @return
	 */
	@Transactional
	public Product update(Product product) {
		return this.productRepository.save(product);
	}
	
	/**
	 * Método que realiza la acción de eliminar los datos de un producto
	 * @param product
	 */
	@Transactional
	public void delete(Product product) {
		this.productRepository.delete(product);
	}
	
	/**
	 * Método que realiza la acción de obtener los productos donde provider__nit =: provider__nit
	 * @param product
	 * @return
	 */	
	public List<Product> findByProviderNit(String provider__nit){
		return this.productRepository.findByProductProviderNit(provider__nit);
	}
}
