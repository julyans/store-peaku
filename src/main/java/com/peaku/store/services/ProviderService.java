/**
 * 
 */
package com.peaku.store.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.peaku.store.model.Provider;
import com.peaku.store.repositories.ProviderRepository;

/**
 * Clase que define los servicios de provider
 * @author Julián Mora
 *
 */

@Service
public class ProviderService {
	
	private final ProviderRepository providerRepository;
	
	public ProviderService(ProviderRepository providerRepository) {
		this.providerRepository = providerRepository;
	}
	
	/**
	 * Método que realiza la acción de guardar los datos de un proveedor
	 * @param provider
	 * @return
	 */
	public Provider create(Provider provider) {
		return this.providerRepository.save(provider);
	}
	
	/**
	 * Método que realiza la acción de actualizar los datos de un proveedor
	 * @param provider
	 * @return
	 */
	public Provider update(Provider provider) {
		return this.providerRepository.save(provider);
	}
	
	/**
	 * Método que realiza la acción de eliminar los datos de un proveedor
	 * @param provider
	 */
	public void delete(Provider provider) {
		this.providerRepository.delete(provider);
	}
	
	/**
	 * Método que realiza la acción de buscar un proveedor en especifico
	 * @param provider
	 */
	public Provider findByProviderNit(int id) {
		return this.providerRepository.findByProviderNit(id);
	}
	
	/**
	 * Método que realiza la acción de obtener todos los proveedores
	 * @param provider
	 */
	public List<Provider> findAll() {
		return this.providerRepository.findAll();
	}
}
